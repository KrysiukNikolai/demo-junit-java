public class Calculator {
    private int _a;
    private int _b;

    public Calculator() {
    }



    public Calculator(int _a, int _b) {
        this._a = _a;
        this._b = _b;
    }

    public double sum(double a, double b) {
        return a + b;
    }

    public int subtract(int a, int b) {
        return a - b;
    }

    public long mult(long a, long b) {
        return a * b;
    }

    public float divide(float a, float b) {
        return a / b;
    }

    public int sum() {
        return _a + _b;
    }

    public int subtract(){
        return _a - _b;
    }
    public int mult(){
       return  _a * _b;
    }
}
