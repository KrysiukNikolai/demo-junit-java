import org.junit.Assert;
import org.junit.Test;

public class CalculatorTest {
    @Test
    public void testSumOne() {
        Calculator calculator = new Calculator();
        double actual = calculator.sum(2, 2);
        Assert.assertEquals(4, actual, 0.1);
    }

    @Test
    public void testSubtract() {
        Calculator calculator = new Calculator();
        int actual = calculator.subtract(8, 4);
        Assert.assertEquals(4, actual);
    }

    @Test
    public void testMult() {
        Calculator calculator = new Calculator();
        long actual = calculator.mult(50, 50);
        Assert.assertEquals(2500, actual);
    }

    @Test
    public void testDivid() {
        Calculator calculator = new Calculator();
        float actual = calculator.divide(100.0F, 20.0F);
        Assert.assertEquals(5F, actual, 0.0001);
    }

    @Test
    public void testSumTwo() {
        Calculator calculator = new Calculator(11, 6);
        int actual = calculator.sum();
        Assert.assertEquals(17, actual);

    }

    @Test
    public void subTwo() {
        Calculator calculator = new Calculator(15, 5);
        int actual = calculator.subtract();
        Assert.assertEquals(10, actual );
    }
    @Test
    public void testMultTwo(){
        Calculator calculator = new  Calculator(5, 5);
        int actual = calculator.mult();
        Assert.assertEquals(25, actual);
    }
}
